﻿define(["sitecore", "jquery"], function(sitecore, jQuery) {
    var editProductPage = sitecore.Definitions.App.extend({
        initialized: function() {
            this.FamilyComboBox.set('items', [{ itemId: 'Televisions' }, { itemId: 'Tablets' }]);

            if (!this.isInCreationMode()) {
                var id = this.getId();
                if (id != null) {
                    this.fetchProperty(id);
                }
            }
        },

        isInCreationMode: function() {
            return Boolean(sitecore.Helpers.url.getQueryParameters(window.location.href)['create']);
        },

        getId: function () {
            var id = sitecore.Helpers.url.getQueryParameters(window.location.href)['id'];
            if (sitecore.Helpers.id.isId(id)) {
                return id;
            } else {
                return null;
            }
        },

        fetchProperty: function (itemId) {
            var self = this;

            jQuery.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/api/product/details',
                data: { id: itemId },
                cache: false,
                success: function (product) {
                    self.populateForm(product);
                },
                error: function () {
                    console.log('it\'s broken!');
                }
            });
        },

        populateForm: function (product) {
            var self = this;

            self.NameTextBox.set('text', product.Name);
            self.FamilyComboBox.set('selectedValue', product.Family);
            self.PriceTextBox.set('text', product.Price);
            self.BrandTextBox.set('text', product.Brand);
        },

        saveProduct: function () {
            var self = this;

            var product = self.extractProduct();

            if (self.isInCreationMode()) {
                jQuery.ajax({
                    type: 'POST',
                    contentType: "application/json",
                    url: '/api/product/new',
                    data: JSON.stringify(product),
                    success: function () {
                        self.showMessageBar(true);
                    },
                    error: function () {
                        console.log('it\'s broken!');
                    }
                });
            } else {
                var id = self.getId();

                jQuery.ajax({
                    type: 'POST',
                    contentType: "application/json",
                    url: '/api/product/edit?id=' + id,
                    data: JSON.stringify(product),
                    success: function () {
                        self.showMessageBar(false);
                    },
                    error: function () {
                        console.log('it\'s broken!');
                    }
                });
            }
        },

        extractProduct: function() {
            return {
                name: this.NameTextBox.get('text'),
                Family: this.FamilyComboBox.get('selectedItemId'),
                Price: this.PriceTextBox.get('text'),
                Brand: this.BrandTextBox.get('text')
            };
        },

        showMessageBar: function (creation) {
            var self = this;
            var messageText = creation ? 'Item was succesfully created' : 'Item was succesfully updated';
            this.MessageBar.set('notifications', [{ text: messageText, actions: [], closable: false, temporary: false }]);
            this.MessageBar.set('isVisible', true);
            setTimeout(function () {
                self.MessageBar.set('isVisible', false);
            }, 3000);
        }
    });

    return editProductPage;
});
