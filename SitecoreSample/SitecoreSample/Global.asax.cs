﻿using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace SitecoreSample
{
    public class Global : Sitecore.Web.Application
    {
        public void Application_Start()
        {
            MapMvcRoutes(RouteTable.Routes);
            GlobalConfiguration.Configure(ConfigureRoutes);
        }

        private void MapMvcRoutes(RouteCollection routes)
        {
            routes.MapRoute("SampleTraditionalMvcRoute",
                "mvc",
                new {controller = "TraditionalMvc", action = "Index"});
        }

        public static void ConfigureRoutes(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("DefaultApiRoute",
             "api/product/{action}/{id}",
              new { controller = "ProductApi", id = RouteParameter.Optional });

            // Attribute routing doen't work out of the box in Sitecore 8...
            //GlobalConfiguration.Configuration.MapHttpAttributeRoutes();
            GlobalConfiguration.Configuration.Formatters.Clear();
            GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());
        }
        
    }
}