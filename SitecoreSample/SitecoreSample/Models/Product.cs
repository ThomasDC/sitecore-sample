﻿namespace SitecoreSample.Models
{
    public class Product
    {
        public string Name { get; set; }
        public string Family { get; set; }
        public string Brand { get; set; }
        public int Price { get; set; }

        public string SitecoreName { get; set; }
    }
}