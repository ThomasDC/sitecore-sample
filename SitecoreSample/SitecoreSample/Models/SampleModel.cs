﻿using System;

namespace SitecoreSample.Models
{
    public class SampleModel
    {
        public DateTime CurrentTime { get; set; }
        public int NumberOfProductsInMasterDatabase { get; set; }
        public int NumberOfProductsInWebDatabase { get; set; }
    }
}