﻿using System.Collections.Generic;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Mvc.Presentation;
using SitecoreSample.Models;

namespace SitecoreSample.Controllers
{
    /// <summary>
    /// Used by Sitecore as an MVC controller
    /// </summary>
    public class ProductListController : Controller
    {
        public ActionResult List()
        {
            var products = new List<Product>();

            foreach (Item productItem in Sitecore.Context.Database.GetItem(new ID(RenderingContext.Current.Rendering.DataSource)).Children)
            {
                products.Add(new Product
                {
                    SitecoreName = productItem.Name,
                    Brand = productItem["Brand"],
                    Price = int.Parse(productItem["Price"]),
                    Family = productItem["Family"],
                    Name = productItem["Name"]
                });
            }

            return View("~/Views/Product/List.cshtml", products);
        }
    }
}