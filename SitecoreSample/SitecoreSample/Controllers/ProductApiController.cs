﻿using System.Diagnostics;
using System.Web.Http;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.SecurityModel;
using SitecoreSample.Models;

namespace SitecoreSample.Controllers
{
    /// <summary>
    /// Traditional ASP.NET Web API controller
    /// </summary>
    public class ProductApiController : ApiController
    {
        [HttpGet]
        public Product Details(string id)
        {
            var product = new Product();

            if (ID.IsID(id))
            {
                var db = Sitecore.Configuration.Factory.GetDatabase("master");

                if (db != null)
                {
                    var item = db.GetItem(ID.Parse(id));
                    product.Price = int.Parse(item.Fields["Price"].Value);
                    product.Name = item.Fields["Name"].Value;
                    product.Family = item.Fields["Family"].Value;
                    product.Brand = item.Fields["Brand"].Value;
                }
            }

            return product;
        }

        [HttpPost]
        [ActionName("edit")]
        public void SaveProduct(Product product, string id)
        {
            var db = Sitecore.Configuration.Factory.GetDatabase("master");

            using (new SecurityDisabler())
            {
                var item = db.GetItem(ID.Parse(id));
                SaveProduct(product, item);
            }
        }

        private void SaveProduct(Product product, Item item)
        {
            using (new EditContext(item))
            {
                item.Fields["Price"].Value = product.Price + "";
                item.Fields["Name"].Value = product.Name;
                item.Fields["Family"].Value = product.Family;
                item.Fields["Brand"].Value = product.Brand;
            }
        }

        [HttpPost]
        [ActionName("new")]
        public void AddNewProduct(Product product)
        {
            using (new SecurityDisabler())
            {
                var master = Sitecore.Configuration.Factory.GetDatabase("master");
                var parent = master.GetItem("/sitecore/content/Home/Products");
                var productTemplateId = new TemplateID(new ID("{BDB51ECE-94AE-48E2-A08B-7B3793517526}"));
                var name = string.Format("{0}-{1}", product.Brand, product.Name).Replace(' ', '-').ToLower();
                var nameProcessed = ItemUtil.ProposeValidItemName(name);

                var newItem = parent.Add(nameProcessed, productTemplateId);

                SaveProduct(product, newItem);
            }
        }
    }
}