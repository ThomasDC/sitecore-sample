﻿using System;
using System.Linq;
using System.Web.Mvc;
using SitecoreSample.Models;

namespace SitecoreSample.Controllers
{
    /// <summary>
    /// Traditional ASP.NET MVC controller
    /// </summary>
    public class TraditionalMvcController : Controller
    {
        public ActionResult Index()
        {
            var model = new SampleModel
            {
                CurrentTime = DateTime.Now
            };

            model.NumberOfProductsInWebDatabase = FetchNumberOfProducts("web");
            model.NumberOfProductsInMasterDatabase = FetchNumberOfProducts("master");

            return View(model);
        }

        private int FetchNumberOfProducts(string database)
        {
            var db = Sitecore.Configuration.Factory.GetDatabase(database);
            return db.SelectItems("fast:/sitecore/content/Home/Products/*[@@templateid='{BDB51ECE-94AE-48E2-A08B-7B3793517526}']").Count();
        }
    }
}